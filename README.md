The **react-pure-loadable** is a lightweight library which provides lazy loading and code splitting through dynamic imports. It is a very thin wrapper around React's Suspense and Lazy API, when used as recommended in the <a href="https://reactjs.org/docs/code-splitting.html">React docs</a>.

# Example
```javascript
  // accordion/index.js
  import PureLoadable from 'react-pure-loadable';

  export const Accordion = PureLoadable({
    loader: () => import('./Accordion'),
    loading: Spinner, // Optional: Defaults to null
    displayName: 'Accordion', // Optional: The component will be easier to find in React Developer Tools
  });
```

# Installation
```npm i --save react-pure-loadable```

or

```yarn add react-pure-loadable```

The library depends on a peer dependency of React 16.6+, which was when Suspense was introduced.

# Why PureLoadable
As shown in the example, PureLoadable uses the familiar interface of the <a href="https://github.com/jamiebuilds/react-loadable">react-loadable</a>. This makes it easy to switch from react-loadable if you already use it.

React-loadable was once the most popular library of this purpose, but it has not been maintained since July 2019, and it generates warnings about using the obsolete React API componentWillMount etc. With PureLoadable you are using the latest React API.

PureLoadable uses a smart way to catch both rendering and asynchronous import errors, so your UI won't crash if any of these happen.

It is also well covered with unit tests (see the source).

# Implementation
The implementation of this library mostly follows the recommendations from React docs and adds a minimum increment to the bundle size. The following is the core of the implementation:

```jsx
  const TargetComponent = React.lazy(() => loader().catch(setLoaderError));
  ...
  return (
    <React.Suspense fallback={<LoadingComponent error={null} />}>
      <TargetComponent {...this.props} />
    </React.Suspense
  );
```

# Concerns
* React.Suspense and React.Lazy are not compatible with server-side rendering, as explained in the <a href="https://reactjs.org/docs/code-splitting.html">React docs</a>. They advise using <a href="https://github.com/gregberge/loadable-components">Loadable components</a> for this purpose.
* React-pure-loadable does not support some advance API that the original react-loadable supports. This was a design decision, to make it as simple as possible, both from the user point of view, and to avoid complicating the code which could create unnecessary bugs.
