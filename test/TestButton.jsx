import React from 'react';

export default ({ childTestId }) => (
  <div>
    <button data-testid={childTestId}>Click me</button>
  </div>
);
