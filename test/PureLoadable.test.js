/* global expect, it, describe */
import React, { useState } from 'react';
import PureLoadable from '../src/PureLoadable';
import { render, fireEvent } from '@testing-library/react';

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));

const ignoreWindowErrors = event => event.preventDefault();

describe('PureLoadable', () => {
  beforeEach(() => {
    window.addEventListener('error', ignoreWindowErrors);
  });

  afterEach(() => {
    window.removeEventListener('error', ignoreWindowErrors);
  });

  it('Loads a good component and renders it', async () => {
    const Button = PureLoadable({
      loader: () => import('./TestButton'),
    });
    const { findByTestId } = render(<Button childTestId="test-id" />);
    await findByTestId('test-id');
  });

  it('Catches an exception thrown from the import function', async () => {
    const Loading = ({ error }) => {
      if (error) {
        return <div data-testid="loading-error-id" />;
      }
      return null;
    };
    const Button = PureLoadable({
      loader: () => Promise.reject('bla'),
      loading: Loading,
    });
    const { findByTestId } = render(<Button errorId="error-id" />);
    await findByTestId('loading-error-id');
  });

  it('Catches an unknown reference thrown from within the target component render func', async () => {
    const Loading = ({ error }) => {
      if (error) {
        return <div data-testid="loading-error-id" />;
      }
      return null;
    };
    const Button = PureLoadable({
      loader: () => import('./UnknownRefTestButton'),
      loading: Loading,
    });
    const { findByTestId } = render(<Button errorId="error-id" />);
    await findByTestId('loading-error-id');
  });

  it('Catches a syntax error thrown from within the target component render func', async () => {
    const Loading = ({ error }) => {
      if (error) {
        return <div data-testid="loading-error-id" />;
      }
      return null;
    };
    const Button = PureLoadable({
      loader: () => import('./SyntaxErrorTestButton'),
      loading: Loading,
    });
    const { findByTestId } = render(<Button errorId="error-id" />);
    await findByTestId('loading-error-id');
  });

  it('Catches an invalid component thrown from within the target component render func', async () => {
    const Loading = ({ error }) => {
      if (error) {
        return <div data-testid="loading-error-id" />;
      }
      return null;
    };
    const Button = PureLoadable({
      loader: () => import('./InvalidComponentTestButton'),
      loading: Loading,
    });
    const { findByTestId } = render(<Button errorId="error-id" />);
    await findByTestId('loading-error-id');
  });

  it('Catches an error when imported file does not exist', async () => {
    const Loading = ({ error }) => {
      if (error) {
        return <div data-testid="loading-error-id" />;
      }
      return null;
    };
    const Button = PureLoadable({
      loader: () => import('./NonExistingFile'),
      loading: Loading,
    });
    const { findByTestId } = render(<Button errorId="error-id" />);
    await findByTestId('loading-error-id');
  });

  it('Keeps the same class once it is imported and created', async () => {
    const Button = PureLoadable({
      loader: () => import('./StatefullButton'),
    });
    const Container = () => {
      const [isClicked1, setIsClicked1] = useState(false);
      const [isClicked2, setIsClicked2] = useState(false);
      return (
        <React.Fragment>
          <Button
            dataTestId="button1"
            onClick={() => {
              setIsClicked1(true);
            }}
          />
          <Button
            dataTestId="button2"
            onClick={() => {
              setIsClicked2(true);
            }}
          />
          {isClicked1 && <div data-testid="clicked-1" />}
          {isClicked2 && <div data-testid="clicked-2" />}
        </React.Fragment>
      );
    };
    const { findByTestId, queryByTestId } = render(<Container />);
    await sleep(200);
    const button1 = queryByTestId('button1');
    const button2 = queryByTestId('button2');
    fireEvent.click(button1);
    fireEvent.click(button2);
    await sleep(200);
    await findByTestId('clicked-1');
    await findByTestId('clicked-2');
  });

  it('Renders the Loading component before the importing is done', async () => {
    const Loading = () => <div data-testid="loading-id" />;
    const Button = PureLoadable({
      loader: () => new Promise(resolve => setTimeout(resolve, 300)),
      loading: Loading,
    });
    const { findByTestId } = render(<Button />);
    await findByTestId('loading-id');
  });
});
