import React from 'react';

export default ({ dataTestId, onClick }) => <button data-testid={dataTestId} onClick={onClick} />;
